#!/usr/bin/python3

'''
multiplication curves

inspired by:
http://superdecade.blogspot.co.uk/2017/12/advent-calendar-for-geeks-21.html
https://youtu.be/qhbuKbxJsk8

James Edmundson 12/2017
james-edmundson.com
'''

import turtle  as t
import math
import time
import datetime
import random
import sys

numberOfPoints=300  #number of points
radiusOfCircle=400   #radius of circle
t.speed(0)

def polToCart(radiusOfCircle,theta):                   #function to convert a polar coordinate to a
	coord=[]
	coord.append(radiusOfCircle*math.cos(math.radians(theta)))    #cartesian coordinate that turtle can follow
	coord.append(radiusOfCircle*math.sin(math.radians(theta)))
	return coord    #returns the coordinate in cartesian form

def loopMult(numberOfPoints,d,radiusOfCircle,multupto):
	for i in range(multupto):
		specific(numberOfPoints,d,radiusOfCircle,i) #draws each multiplication curve up to the value specified
		time.sleep(0.7)
		t.clear()

def specific(numberOfPoints,d,radiusOfCircle,specificMultiple):
	t.tracer(0,0)               #for speeding up turtle graphics
	for i in range(numberOfPoints):          #for each point on the circumference of the cirle
		point=(specificMultiple*i)%numberOfPoints                  #determins which point the line should go to
		t.goto(polToCart(radiusOfCircle,i*d))  #goes to the next point
		t.pd()
		t.goto(polToCart(radiusOfCircle,point*d))  #goes to the new point
		t.pu()
	t.update() #updates turtle window, so all lines are drawn

def screenCap():
	ts = t.getscreen()
	ts.getcanvas().postscript(file="{}.eps".format(datetime.datetime.now().strftime("%Y-%m-%d%H:%M"))) # saves the turtle screen as the date and time.eps

def init(radiusOfCircle): #changing turtle settings ready for drawing
	t.bgcolor("Black")	#changing the background colour to black
	t.color("Red")		#turtle pen colour to red
	t.ht()			#hiding the turtle character itself
	t.penup()		#pen up
	t.goto(polToCart(radiusOfCircle, 0)) #going to the first point on the circle
	t.pd() #pen down

def main(numberOfPoints=numberOfPoints, radiusOfCircle=radiusOfCircle): #defaults to global variables, but can be set as otherwise
	menuOption = input("> ")
	if menuOption == "s": #specific curve drawn
		try:
			multiple = int(input("multiple: ")) #getting the multiple
			init(radiusOfCircle)				#making turtle pretty
			specific(numberOfPoints, 360/numberOfPoints, radiusOfCircle, multiple) #drawing the specific curve
		except ValueError:	#if the user's naughty
			print("Multiple needs to be an int!")
	elif menuOption == "l": #drawing many curves in a loop
		try:
			upto = int(input("Loop up to: "))
			init(radiusOfCircle)
			loopMult(numberOfPoints, 360/numberOfPoints, radiusOfCircle, upto)
		except ValueError:
			print("Multiple needs to be an int!")
	elif menuOption == "c": #clears turtle screen
		t.clear()
	elif menuOption == "p": #takes a screen grab of the turtle window
		screenCap()
	elif menuOption == "h" or menuOption == "help": #prints help commands
		print("s for specific pattern\nl for looping multiples\nc to clear turtle\np to take a picture\nh for this help\nq to quit")
	elif menuOption == "q": #quit
		sys.exit()
	else:
		print("NOT RECOGNISED")

if __name__ == '__main__':
	while True:
		main()
